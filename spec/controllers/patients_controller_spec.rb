require 'rails_helper'

RSpec.describe PatientsController do
  before do
    @one = {
      atrributes: {
        first_name: 'Thomas',
        last_name: 'Schudel',
        gender: 0,
        medical_record: 30_997,
        birth_date: '1975-03-03 00:00:00'
      },
      admission: {
        moment: '2018-02-18 17:07:00',
        diagnoses: [{ code: 'F11.101', description: 'fracture of left hand' }],
        symptoms: [
          { description: 'severe pain' },
          { description: 'swelling' },
          { description: 'limited bending of the joint' }
        ],
        observations: [{ description: 'No soft tissues were damaged', moment: DateTime.new(2018, 0o2, 18, 18, 0) }]
      },
      allergies: [
        { description: 'hypersensitivity to aspirin or NSAIDs' },
        { description: 'gluten intolerance' }
      ],
      diagnoses: [
        { code: 'J45', description: 'Asthma', is_chronic_condition: true },
        { code: 'S82.101A', description: 'a closed fracture in the right tibia' }
      ],
      medication_orders: [
        {
          name: 'Acetaminophen',
          dosage: 500,
          unit: 0,
          route: 0,
          necessity: 'relieve pain',
          frequency: { order_frequency_value: 'q4', unit: 0 }
        },
        {
          name: 'Naproxen',
          dosage: 500,
          unit: 0,
          route: 0,
          necessity: 'relieve swelling',
          frequency: { order_frequency_value: 'q6', unit: 0 }
        }
      ],
      diagnostic_procedures: [
        { description: 'an exploratory radiography', moment: DateTime.new(2018, 0o2, 18, 18, 30) }
      ],
      treatments: [
        { description: 'temporary bracing the right leg', necessity: 'restrict the motion' }
      ]
    }
  end
  describe 'GET #index' do
    let(:patients) { [create_full_patient(@one)] }

    it 'renders index view' do
      get :index
      expect(response).to render_template :index
    end

    it 'populates an array of patients' do
      get :index
      expect(assigns(:patients)).to match_array(patients)
    end
  end

  describe 'GET #sumary' do
    let(:facility) { create(:facility, name: 'Blue Alps Ski Camp') }

    let(:patient) { create_full_patient(@one) }

    it 'assigns the requested record to @patient' do
      get :summary, params: { id: patient.id }
      expect(assigns(:patient)).to eq patient
    end

    it 'renders the #summary view' do
      get :summary, params: { id: patient.id }
      expect(response).to render_template :summary
    end

    it 'loads the right facility' do
      get :summary, params: { id: patient.id }
      expect(facility.name).to eq 'Blue Alps Ski Camp'
    end

    it 'loads the right patient' do
      get :summary, params: { id: patient.id }
      expect(patient.first_name).to eq 'Thomas'
      expect(patient.last_name).to eq 'Schudel'
      expect(patient.full_name).to eq 'Thomas A Schudel'
      expect(patient.gender).to eq 'male'
      expect(patient.medical_record).to eq '30997'
      expect(patient.birth_date).to eq '1975-03-03 00:00:00'
      expect(patient.age).to eq 43
    end

    it 'has an admission' do
      get :summary, params: { id: patient.id }
      expect(patient.admission).to be_present
    end

    it 'loads admission attributes' do
      get :summary, params: { id: patient.id }
      expect(patient.admission.moment).to be_present
      expect(patient.admission.diagnoses).to be_present
      expect(patient.admission.symptoms).to be_present
      expect(patient.admission.observations).to be_present
    end

    it 'has the right admission attributes' do
      get :summary, params: { id: patient.id }
      expect(patient.admission.moment).to eq '2018-02-18 17:07:00'
    end

    it 'checks if associations has right sizes' do
      get :summary, params: { id: patient.id }
      expect(patient.admission.diagnoses.count).to eq 2
      expect(patient.admission.symptoms.count).to eq 3
      expect(patient.admission.observations.count).to eq 1
    end

    it 'checks if patient has allergies' do
      get :summary, params: { id: patient.id }
      expect(patient.allergies).to be_present
      expect(patient.allergies.count).to eq 2
    end

    it 'checks if patients has diagnoses' do
      get :summary, params: { id: patient.id }
      expect(patient.diagnoses).to be_present
    end

    it 'checks if patients has chronic conditions' do
      get :summary, params: { id: patient.id }
      expect(patient.diagnoses.find(&:is_chronic_condition)).to_not be_nil
    end

    it 'checks if patients has medication orders' do
      get :summary, params: { id: patient.id }
      expect(patient.medication_orders).to be_present
    end

    it 'check if medication orders has frequencies' do
      get :summary, params: { id: patient.id }
      expect(patient.medication_orders.find(&:frequency)).to_not be_nil
    end

    it 'checks if patients has diagnostic procedures' do
      get :summary, params: { id: patient.id }
      expect(patient.diagnostic_procedures).to be_present
    end

    it 'checks if patients has treatments' do
      get :summary, params: { id: patient.id }
      expect(patient.treatments).to be_present
    end
  end
end
