require 'factory_bot'
require 'support/test_helpers'

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.include TestHelpers
end
