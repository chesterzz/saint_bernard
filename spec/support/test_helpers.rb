module TestHelpers
  include FactoryBot::Syntax::Methods

  def create_full_patient(patient)
    temp_admission = {
      moment: patient[:admission].delete(:moment),
      diagnoses: extract_and_create(patient[:admission].delete(:diagnoses), :diagnosis),
      symptoms: extract_and_create(patient[:admission].delete(:symptoms), :symptom),
      observations: extract_and_create(patient[:admission].delete(:observations), :observation)
    }
    temp_patient = {
      allergies: extract_and_create(patient.delete(:allergies), :allergy),
      diagnoses: extract_diagnoses(patient.delete(:diagnoses)),
      medication_orders: extract_medication_orders(patient.delete(:medication_orders)),
      diagnostic_procedures: extract_and_create(patient.delete(:diagnostic_procedures), :diagnostic_procedure),
      treatments: extract_and_create(patient.delete(:treatments), :treatment)
    }

    create(
      :full_patient,
      patient[:attributes],
      admission: create(:full_admission, temp_admission),
      allergies: temp_patient[:allergies],
      diagnoses: temp_patient[:diagnoses]
    )
  end

  def extract_medication_orders(medication_orders)
    result = []
    medication_orders.each do |medication_order|
      temp = medication_order.delete(:frequency)
      frequency = create(:order_frequency, order_frequency_value: temp[:order_frequency_value], unit: temp[:unit])
      result << create(:full_medication_order, name: medication_order[:name], dosage: medication_order[:dosage], unit: medication_order[:unit], route: medication_order[:route], necessity: medication_order[:necessity], frequency: frequency)
    end
    result
  end

  def extract_diagnoses(diagnoses)
    result = []
    diagnoses.each do |diagnosis|
      result << if diagnosis[:is_chronic_condition].present?
                  create(:diagnosis, :chronic_condition)
                else
                  create(:diagnosis)
                end
    end
    result
  end

  def extract_and_create(collection, type)
    result = []
    collection.each do |item|
      case type
      when  :diagnosis
        attrs = {
          code: item[:code],
          description: item[:description]
        }
      when  :symptom,
            :observation,
            :allergy,
            :diagnostic_procedure,
            :treatment
        attrs = {
          description: item[:description]
        }
      end
      result << create(type, attrs)
    end
    result
  end
end
