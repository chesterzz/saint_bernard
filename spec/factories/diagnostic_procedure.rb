require 'rails_helper'

FactoryBot.define do
  factory :diagnostic_procedure do
    sequence(:description) { |n| "Allergy #{n}" }
    moment { DateTime.now }
  end
end
