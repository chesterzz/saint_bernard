require 'rails_helper'

FactoryBot.define do
  factory :admission do
    moment { '2018-02-18 17:07:00' }

    factory :full_admission do
      before(:create) do |admission|
        admission.diagnoses = create_list(:diagnosis, 2)
        admission.symptoms = create_list(:symptom, 3)
        admission.observations = create_list(:observation, 1)
      end
    end
  end
end
