require 'rails_helper'

FactoryBot.define do
  factory :symptom do
    sequence(:description) { |n| "Symptom #{n}" }
  end
end
