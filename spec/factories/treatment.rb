require 'rails_helper'

FactoryBot.define do
  factory :treatment do
    sequence(:description) { |n| "Treatment #{n}" }
    sequence(:necessity) { |n| "to necessity #{n}" }
  end
end
