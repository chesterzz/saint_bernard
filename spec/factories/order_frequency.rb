require 'rails_helper'

FactoryBot.define do
  factory :order_frequency do
    sequence(:order_frequency_value) { |n| "q#{n}" }
    unit { 0 }
  end
end
