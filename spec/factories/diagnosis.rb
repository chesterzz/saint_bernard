require 'rails_helper'

FactoryBot.define do
  factory :diagnosis do
    sequence(:code) { |n| "CODE.#{n}" }
    sequence(:description) { |n| "Diagnosis #{n}" }

    trait :chronic_condition do
      is_chronic_condition { true }
    end
  end
end
