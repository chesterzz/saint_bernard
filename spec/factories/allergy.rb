require 'rails_helper'

FactoryBot.define do
  factory :allergy do
    sequence(:description) { |n| "Allergy #{n}" }
  end
end
