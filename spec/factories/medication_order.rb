require 'rails_helper'

FactoryBot.define do
  factory :medication_order do
    sequence(:name) { |n| "Medication Order #{n}" }
    dosage { 500 }
    unit { 0 }
    route { 0 }
    sequence(:necessity) { |n| "Necessity #{n}" }

    factory :full_medication_order do
      before :create do |medication_order|
        medication_order.frequency = create(:order_frequency)
      end
    end
  end
end
