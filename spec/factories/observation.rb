require 'rails_helper'

FactoryBot.define do
  factory :observation do
    sequence(:description) { |n| "Observation #{n}" }
    moment { DateTime.now }
  end
end
