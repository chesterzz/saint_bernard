require 'rails_helper'

FactoryBot.define do
  factory :patient do
    first_name { 'Thomas' }
    middle_name { 'A' }
    last_name { 'Schudel' }
    gender { 0 }
    medical_record { 30_997 }
    birth_date { '1975-03-03 00:00:00' }

    factory :full_patient do
      before :create do |patient|
        patient.admission = create(:full_admission)
        patient.diagnoses = [
          create(:diagnosis),
          create(:diagnosis, :chronic_condition)
        ]
        patient.allergies = create_list(:allergy, 2)
        patient.medication_orders = create_list(:full_medication_order, 2)
        patient.diagnostic_procedures = create_list(:diagnostic_procedure, 2)
        patient.treatments = create_list(:treatment, 2)
      end
    end
  end
end
