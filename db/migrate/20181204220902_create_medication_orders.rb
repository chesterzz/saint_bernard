class CreateMedicationOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :medication_orders do |t|
      t.column :name, :string
      t.column :unit, :integer
      t.column :dosage, :decimal, precision: 6, scale: 2
      t.column :route, :integer
      t.column :necessity, :text
      t.column :patient_id, :integer, index: true
      t.timestamps
    end
  end
end
