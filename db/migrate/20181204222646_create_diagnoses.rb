class CreateDiagnoses < ActiveRecord::Migration[5.1]
  def change
    create_table :diagnoses do |t|
      t.column :coding_system, :string
      t.column :code, :string
      t.column :description, :text
      t.column :is_chronic_condition, :boolean, default: false
      t.column :patient_id, :integer, index: true
      t.column :admission_id, :integer, index: true
      t.timestamps
    end
  end
end
