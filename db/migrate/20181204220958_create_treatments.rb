class CreateTreatments < ActiveRecord::Migration[5.1]
  def change
    create_table :treatments do |t|
      t.column :description, :text
      t.column :necessity, :text
      t.column :patient_id, :integer, index: true
      t.timestamps
    end
  end
end
