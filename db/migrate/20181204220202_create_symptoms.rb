class CreateSymptoms < ActiveRecord::Migration[5.1]
  def change
    create_table :symptoms do |t|
      t.column :description, :text
      t.column :admission_id, :integer, index: true
      t.timestamps
    end
  end
end
