class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.column :first_name, :string
      t.column :middle_name, :string
      t.column :last_name, :string
      t.column :medical_record, :string
      t.column :birth_date, :datetime
      t.column :gender, :integer
      t.column :admission_id, :integer
      t.timestamps
    end
  end
end
