class CreateDiagnosticProcedures < ActiveRecord::Migration[5.1]
  def change
    create_table :diagnostic_procedures do |t|
      t.column :description, :text
      t.column :moment, :datetime
      t.column :patient_id, :integer, index: true
      t.timestamps
    end
  end
end
