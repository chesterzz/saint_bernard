class CreateAllergies < ActiveRecord::Migration[5.1]
  def change
    create_table :allergies do |t|
      t.column :description, :text
      t.column :patient_id, :intenger, index: true
      t.timestamps
    end
  end
end
