class CreateOrderFrequencies < ActiveRecord::Migration[5.1]
  def change
    create_table :order_frequencies do |t|
      t.column :order_frequency_value, :string
      t.column :unit, :integer
      t.column :medication_order_id, :integer, index: true
      t.timestamps
    end
  end
end
