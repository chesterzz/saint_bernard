class CreateObservations < ActiveRecord::Migration[5.1]
  def change
    create_table :observations do |t|
      t.column :description, :text
      t.column :moment, :datetime
      t.column :admission_id, :integer, index: true
      t.timestamps
    end
  end
end
