class CreateAdmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :admissions do |t|
      t.column :moment, :datetime
      t.column :patient_id, :integer, index: true
      t.timestamps
    end
  end
end
