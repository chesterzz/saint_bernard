ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.tables.each do |table|
  ActiveRecord::Base.connection.execute("DELETE FROM #{table}") unless table == 'schema_migrations'
end

Facility.create(name: 'Blue Alps Ski Camp')

# 1
Patient.create(
  first_name: 'Thomas', last_name: 'Schudel', gender: 0, medical_record: 30_997, birth_date: '1975-03-09 00:00:00',
  admission: Admission.create(
    moment: '2018-02-18 17:07:00',
    diagnoses: [
      Diagnosis.create(code: 'F11.101', description: 'fracture of left hand')
    ],
    symptoms: [
      Symptom.create(description: 'severe pain'),
      Symptom.create(description: 'swelling'),
      Symptom.create(description: 'limited bending of the joint')
    ],
    observations: [
      Observation.create(description: 'No soft tissues were damaged', moment: DateTime.now)
    ]
  ),
  allergies: [
    Allergy.create(description: 'hypersensitivity to aspirin or NSAIDs'),
    Allergy.create(description: 'gluten intolerance')
  ],
  diagnoses: [
    Diagnosis.create(code: 'J45', description: 'Asthma', is_chronic_condition: true),
    Diagnosis.create(code: 'S82.101A', description: 'a closed fracture in the right tibia')
  ],
  medication_orders: [
    MedicationOrder.create(
      name: 'Acetaminophen', dosage: 500, unit: 0, route: 0, necessity: 'relieve pain',
      frequency: OrderFrequency.create(order_frequency_value: 'q4', unit: 0)
    ),
    MedicationOrder.create(
      name: 'Naproxen', dosage: 500, unit: 0, route: 0, necessity: 'relieve swelling',
      frequency: OrderFrequency.create(order_frequency_value: 'q6', unit: 0)
    )
  ],
  diagnostic_procedures: [
    DiagnosticProcedure.create(description: 'an exploratory radiography', moment: '2018-08-12 17:15')
  ],
  treatments: [
    Treatment.create(description: 'temporary bracing the right leg', necessity: 'restrict the motion')
  ]
)
