ActionView::Base.field_error_proc = proc do |html_tag, instance|
  %(
    <div class="has-error">
      #{html_tag}
      <p class="help-block">#{instance.error_message.map(&:capitalize).to_sentence}</p>
    </div>
  ).html_safe
end
