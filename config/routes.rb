Rails.application.routes.draw do
  root 'dashboard#index'

  resources :patients
  get 'summary/:id', to: 'patients#summary', as: :summary
end
