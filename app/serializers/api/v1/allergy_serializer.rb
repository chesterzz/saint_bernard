class API::V1::AllergySerializer < ActiveModel::Serializer
  type :allergy
  attributes :id, :description
end
