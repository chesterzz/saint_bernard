class API::V1::PatientSerializer < ActiveModel::Serializer
  type :patient
  attributes :id, :full_name, :medical_record, :age, :gender, :admission, :allergies, :chronic_conditions, :medications, :diagnostic_procedures, :diagnoses, :treatments

  def gender
    get_from_locale('gender')
  end

  def admission
    object.admission ? API::V1::AdmissionSerializer.new(object.admission) : nil
  end

  def allergies
    object.allergies ? ActiveModel::Serializer::CollectionSerializer.new(object.allergies, serializer: API::V1::AllergySerializer) : []
  end

  def chronic_conditions
    object.chronic_conditions ? ActiveModel::Serializer::CollectionSerializer.new(object.chronic_conditions, serializer: API::V1::DiagnosisSerializer) : []
  end

  def medications
    object.medications ? ActiveModel::Serializer::CollectionSerializer.new(object.medications, serializer: API::V1::MedicationOrderSerializer) : []
  end

  def diagnostic_procedures
    object.diagnostic_procedures ? ActiveModel::Serializer::CollectionSerializer.new(object.diagnostic_procedures, serializer: API::V1::DiagnosticProcedureSerializer) : []
  end

  def diagnoses
    object.diagnoses ? ActiveModel::Serializer::CollectionSerializer.new(object.diagnoses, serializer: API::V1::DiagnosisSerializer) : []
  end

  def treatments
    object.treatments ? ActiveModel::Serializer::CollectionSerializer.new(object.treatments, serializer: API::V1::TreatmentSerializer) : []
  end

  private

  def get_from_locale(attribute)
    object.read_attribute(attribute) ? I18n.t("models.patient.gender.#{object.read_attribute(attribute)}") : nil
  end
end
