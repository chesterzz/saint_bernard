class API::V1::TreatmentSerializer < ActiveModel::Serializer
  type :treatment
  attributes :id, :description, :necessity
end
