class API::V1::SymptomSerializer < ActiveModel::Serializer
  type :symptom
  attributes :id, :description
end
