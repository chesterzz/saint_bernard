class API::V1::AdmissionSerializer < ActiveModel::Serializer
  type :admission
  attributes :id, :moment, :diagnoses, :symptoms, :observations

  def diagnoses
    object.diagnoses ? ActiveModel::Serializer::CollectionSerializer.new(object.diagnoses, serializer: API::V1::DiagnosisSerializer) : []
  end

  def symptoms
    object.symptoms ? ActiveModel::Serializer::CollectionSerializer.new(object.symptoms, serializer: API::V1::SymptomSerializer) : []
  end

  def observations
    object.observations ? ActiveModel::Serializer::CollectionSerializer.new(object.observations, serializer: API::V1::ObservationSerializer) : []
  end
end
