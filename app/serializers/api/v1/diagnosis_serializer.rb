class API::V1::DiagnosisSerializer < ActiveModel::Serializer
  type :diagnosis
  attributes :id, :coding_system, :code, :description
end
