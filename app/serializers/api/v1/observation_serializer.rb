class API::V1::ObservationSerializer < ActiveModel::Serializer
  type :observation
  attributes :id, :description, :moment
end
