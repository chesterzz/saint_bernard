class API::V1::OrderFrequencySerializer < ActiveModel::Serializer
  type :order_frequency
  attributes :id, :order_frequency_value, :unit

  def unit
    get_from_locale('unit')
  end

  private

  def get_from_locale(attribute)
    object.read_attribute(attribute) ? I18n.t("enums.frequency_unit.#{object.read_attribute(attribute)}") : nil
  end
end
