class API::V1::DiagnosticProcedureSerializer < ActiveModel::Serializer
  type :diagnostic_procedure
  attributes :id, :description, :moment
end
