class API::V1::MedicationOrderSerializer < ActiveModel::Serializer
  type :medication_order
  attributes :id, :name, :unit, :dosage, :route, :frequency

  def unit
    get_from_locale('mass_unit', 'unit')
  end

  def route
    get_from_locale('routes', 'route')
  end

  def frequency
    object.frequency ? API::V1::OrderFrequencySerializer.new(object.frequency) : nil
  end

  private

  def get_from_locale(type, attribute)
    object.read_attribute(attribute) ? I18n.t("enums.#{type}.#{object.read_attribute(attribute)}") : nil
  end
end
