class MedicationOrder < ApplicationRecord
  has_one :frequency, foreign_key: 'medication_order_id', class_name: 'OrderFrequency'

  enum unit: [:mg]
  enum route: [:po, :im, :sc]

  validates :name, :unit, :dosage, :route, :necessity, presence: true
  validates :frequency, presence: true
end
