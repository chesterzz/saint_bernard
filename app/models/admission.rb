class Admission < ApplicationRecord
  has_many :diagnoses
  has_many :symptoms
  has_many :observations

  validates :moment, presence: true
  validates :observations, presence: true
  validates :symptoms, presence: true
  validates :diagnoses, presence: true
end
