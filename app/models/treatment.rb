class Treatment < ApplicationRecord
  validates :description, :necessity, presence: true

  def described_treatment
    "#{description} to #{necessity}"
  end
end
