class Diagnosis < ApplicationRecord
  validates :code, :description, presence: true

  def described_code
    "#{description} (#{code})"
  end
end
