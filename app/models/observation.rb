class Observation < ApplicationRecord
  validates :description, :moment, presence: true
end
