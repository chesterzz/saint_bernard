class OrderFrequency < ApplicationRecord
  enum unit: [:hour]

  validates :order_frequency_value, :unit, presence: true
end
