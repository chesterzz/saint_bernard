class Patient < ApplicationRecord
  has_one :admission
  has_many :allergies
  has_many :medication_orders
  has_many :diagnostic_procedures
  has_many :diagnoses
  has_many :treatments

  enum gender: [:male, :female, :other]

  validates :first_name, :last_name, :medical_record, :birth_date, :gender, presence: true

  validates :admission, presence: true
  validates :allergies, presence: true
  validates :medication_orders, presence: true
  validates :diagnostic_procedures, presence: true
  validates :diagnoses, presence: true
  validates :treatments, presence: true

  def age
    0 if birth_date.blank?
    now = Time.now.utc.to_date
    now.year - birth_date.year - (now.month > birth_date.month || (now.month == birth_date.month && now.day >= birth_date.day) ? 0 : 1)
  end

  def simple_diagnoses
    diagnoses.where is_chronic_condition: false
  end

  def chronic_conditions
    diagnoses.where is_chronic_condition: true
  end

  def full_name
    "#{first_name} #{middle_name} #{last_name}"
  end
end
