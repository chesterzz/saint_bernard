class DiagnosticProcedure < ApplicationRecord
  validates :description, :moment, presence: true
end
