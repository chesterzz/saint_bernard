module ApplicationHelper
  def render_flash_messages
    flash_messages = []
    flash.each do |type, message|
      next if message.blank?

      case type.to_sym
      when :success
        type = 'alert-success'
      when :error
        type = 'alert-danger'
      when :alert
        type = 'alert-warning'
      when :notice
        type = 'alert-info'
      end
      Array(message).each do |message_text|
        unless message_text.blank?
          alert_content = %(<div class="alert #{type} alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> #{message_text.html_safe}</div>)
          flash_messages << alert_content
        end
      end
    end
    flash.clear
    flash_messages.join("\n").html_safe
  end

  def render_breadcrumbs
    return if @breadcrumbs.blank?

    @breadcrumbs.insert(0, label: 'Dashboard', url: root_path)
    html = %(<ol class="breadcrumb">)
    @breadcrumbs[0..-2].each do |breadcrumb|
      html += breadcrumb[:url].blank? ? %(<li class="breadcrumb-item#{(' featured' if breadcrumb[:featured])}">#{breadcrumb[:label].html_safe}</li>) : %(<li class="breadcrumb-item#{(' featured' if breadcrumb[:featured])}">#{link_to(breadcrumb[:label].html_safe, breadcrumb[:url], breadcrumb[:options])}</li>)
    end
    html += %(<li class="breadcrumb-item active"><strong>#{@breadcrumbs.last[:label]}</strong></li>)
    html += %(</ol>)
    html.html_safe
  end

  def loader_for_links
    %(<span class='glyphicon glyphicon-cog'></span>)
  end

  def icons(icon)
    %(<span class='glyphicon glyphicon-#{icon}'></span>)
  end
end
