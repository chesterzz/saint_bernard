module PatientsHelper
  def render_medications(medication_orders)
    medications = []
    medication_orders.each do |order|
      medications << "#{order.name} #{order.dosage}#{t("enums.mass_unit.#{order.unit}")} #{t("enums.routes.#{order.route}")} #{order.frequency.order_frequency_value}#{t("enums.frequency_unit.#{order.frequency.unit}")} to #{order.necessity}"
    end
    medications.to_sentence
  end

  def render_diagnostic_procedures(diagnostic_procedures)
    procedures = []
    diagnostic_procedures.each do |procedure|
      procedures << "#{procedure.description} #{l(procedure.moment, format: :hours)}"
    end
    procedures.to_sentence
  end

  def render_patient_summary(facility, patient)
    html = ''
    html << %(<p>)
    html << %(This #{patient.age} years old #{patient.gender} was admitted to #{facility.name}\ )
    html << %(on #{l(patient.admission.moment, format: :complete)}\ )
    html << %(due to #{patient.admission.diagnoses.map(&:described_code).to_sentence}.\ )
    html << %(The observed symptoms on admission were #{patient.admission.symptoms.map(&:description).to_sentence}.\ )
    html << %(#{patient.admission.observations.map(&:description).to_sentence}.)
    html << %(</p>)

    html << %(<p>)
    html << %(Upon asking about known allergies, the patient disclosed #{patient.allergies.map(&:description).to_sentence}.\ )
    html << %(Upon asking about chronic conditions, the patient disclosed\ )
    html << %(#{patient.chronic_conditions.map(&:described_code).to_sentence}.\ )
    html << %(The patient was administered with #{render_medications(patient.medication_orders)}.)
    html << %(</p>)

    html << %(<p>)
    html << %(The staff performed #{render_diagnostic_procedures(patient.diagnostic_procedures)},\ )
    html << %(revealing #{patient.simple_diagnoses.map(&:described_code).to_sentence}.\ )
    html << %(Our team proceeded to #{patient.treatments.map(&:described_treatment).to_sentence}.\ )
    html << %(</p>)

    html.html_safe
  end
end
