module SidebarMenuHelper
  def build_sidemenu(&_block)
    menu = {}

    # Dashboard
    menu_index = 0
    menu[menu_index] = { label: 'Dashboard', index: nil, submenu: [], icon: 'home', url: :root, active: active_controller?('dashboard') }
    menu[menu_index][:index] = menu_index

    # Patients
    menu_index += 1
    menu[menu_index] = { label: 'Patients', index: nil, submenu: [], icon: 'user', active: active_controller?(['patients']) }
    menu[menu_index][:index] = menu_index
    menu[menu_index][:submenu] << { label: 'List', url: :patients }

    menu.each_value do |group|
      yield(group)
    end
  end

  def active_controller?(controller_name)
    if controller_name.is_a?(String)
      params[:controller] == controller_name
    else
      controller_name.include?(params[:controller])
    end
  end

  def active_action?(action_name)
    if action_name.is_a?(String)
      params[:action] == action_name
    else
      action_name.include?(params[:action])
    end
  end
end
