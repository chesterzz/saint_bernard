class DashboardController < ApplicationController
  def index
    @patients = Patient.all.limit(10)
  end
end
