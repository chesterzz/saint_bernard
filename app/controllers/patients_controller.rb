class PatientsController < ApplicationController
  before_action :breadcrumbs
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def index
    @patients = Patient.all
  end

  def summary
    @facility = Facility.first
    @patient = Patient.find(params[:id])
  end

  private

  def record_not_found
    flash[:error] = 'Patient not found'
    redirect_to patients_path
  end

  def breadcrumbs
    @breadcrumbs = [{ label: 'Patients' }]
    @breadcrumbs << { label: 'Summary' } if params[:action] == 'summary'
  end
end
