module ApplicationControl
  extend ActiveSupport::Concern

  included do
    # attr_reader :current_user
    # before_action :session_and_stuff
    layout 'application'
  end

  # def session_and_stuff
  #   if session[:cms_user]
  #     @current_user = User.includes(:access_profile).find_by(id: session[:cms_user], status: User.statuses[:active])
  #     # Raven.user_context(id: session[:cms_user])
  #     # Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  #   end
  #   if @current_user.nil?
  #     flash[:alert] = t('alerts.not_logged_in')
  #     redirect_to :cms_root
  #   elsif @current_user.access_profile.can?(:robot, :rpa) && naughty_robot?
  #     redirect_to new_cms_rpa_path
  #   else
  #     @breadcrumbs = []
  #   end
  # end

  def error_summary_message(object)
    Rails.logger.info(object)
    "#{object.errors.count} #{(object.errors.count == 1 ? 'error' : 'errors')} prevented this record from being saved."
  end
end
