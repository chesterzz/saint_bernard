# README

To run the app:
- Clone repository
- Run `bundle install`
- Run `rails db:setup`
- Run `rails s`

For testing:
- Run `rails db:test:prepare`
- Run `rspec`